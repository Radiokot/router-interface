#!/bin/sh
echo "Content-Type: text/html"
echo ""
wrx=$(cat /sys/class/net/eth1/statistics/rx_bytes)
wtx=$(cat /sys/class/net/eth1/statistics/tx_bytes)
echo "WAN: RX ${wrx}, TX ${wtx} <br/>"
wlrx=$(cat /sys/class/net/wlan0/statistics/rx_bytes)
wltx=$(cat /sys/class/net/wlan0/statistics/tx_bytes)
echo "WLAN: RX ${wlrx}, TX ${wltx} <br/>"
