#!/bin/sh
#!/bin/sh
echo "Content-Type: text/html"
echo ""
#получить SSID'ы
interfaces=$(iw dev | grep Interface | cut -f 2 -s -d" ")
interf_count=$(echo $interfaces | wc -w)

if [[ $interf_count -eq 0 ]]; then
	echo "Отключен"
	exit
fi

i=0
for interface in $interfaces
do
	ssid=$(uci get wireless.@wifi-iface[$i].ssid)
	echo "${interface}: $ssid <br>" 
	i=$(($i + 1))
done
#получить канал
channel=$(iw dev wlan0 info | grep channel | cut -c 10-21)
echo "Канал: $channel <br/>"
#получить клиентов со всех wlan'ов
clients=""
c_count=0
for interface in $interfaces
do
  maclist=$(iw dev $interface station dump | grep Station | cut -f 2 -s -d" ")
  for mac in $maclist
  do
	host=""
	host=$(cat /tmp/dhcp.leases | cut -f 2,3,4 -s -d" " | grep $mac | cut -f 3 -s -d" ")
	clients=$(echo -e "$clients $host ($mac - $interface) <br>") #типо сложение строк
	c_count=$(($c_count + 1))
  done
done
if [[ $c_count -gt 0 ]]; then
        echo "Клиенты: $c_count<br/>"
		echo "<div style='padding-left: 10px'>"
		echo -e "$clients"
		echo "</div>"
else
        echo "Клиентов нет"
fi