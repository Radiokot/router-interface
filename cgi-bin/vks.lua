#!/usr/bin/lua

require "socket"
http = require("socket.http")

print("Content-Type: text/event-stream\r\n")

downLimit = 50000000
upLimit = 411000000

uid = tonumber(string.match(os.getenv("QUERY_STRING"),"id=(.+)&t"))
from = tonumber(string.match(os.getenv("QUERY_STRING"),"from=(.+)"))
token = string.match(os.getenv("QUERY_STRING"),"token=(.+)&")

tempUid=","..uid.."_"
errorFlag = false

function findDocs(uid, last, preDocs, token)
	-- работаем через анонимайзер из-за отсутствия https, ответ - json
	url = "http://0s.mfygs.ozvs4y3pnu.cmle.ru/method/execute.findDocs?uid="..uid.."&from="..last.."&preDocs="..preDocs.."&access_token="..token
	vkResponse = http.request(url)
	-- tmr
	if string.match(vkResponse, "\"error_code\":([0-9]+)") == "6" then
		socket.sleep(0.350)
		return findDocs(uid, last, preDocs, token)
	end

	return string.match(vkResponse, "\"found\":(.+])"), string.match(vkResponse, "\"to\":([0-9]+)"), vkResponse
end
-- от новых к старым
last = from
for k=0,30 do
	-- 7 - 400, 8 - 200, 9 - 150, 10 - 100
	last = from - 150
	preDocs = ""
	for i=last,from do
		preDocs = preDocs..tempUid..i
	end
	found, to, all = findDocs(uid, last, preDocs, token)
	-- print("data: {")
	-- print("data: \"range\": \""..from.." - "..to.."\",")
	-- print("data: \"found\": "..found..",")
	-- print("data: \"last\": "..to)
	print("data: "..all.."\n")
	-- print("data: }\n")
	from = to
	io.flush()
	--if errorFlag then break end
end

