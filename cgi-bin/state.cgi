#!/bin/sh
echo "Content-Type: text/html"
echo ""
echo "Хост: $(uci get system.@system[0].hostname) <br/>"
ip=$(ifconfig eth1 | grep "inet addr" | awk -F: '{print $2}' | awk '{print $1}')
echo "IP: $ip <br/>"
su=$(cat /proc/uptime | cut -d'.' -f1-1)
sud=$((su/86400))
su=$((su-(sud*86400)))
suh=$((su/3600))
su=$((su-(suh*3600)))
sum=$((su/60))
echo "Uptime системы: ${sud}д ${suh}ч ${sum}м<br/>"
wu=$(ubus call network.interface.wan status | grep uptime | awk '{print $2}' | tr ',' ' ')
wud=$((wu/86400))
wu=$((wu-(wud*86400)))
wuh=$((wu/3600))
wu=$((wu-(wuh*3600)))
wum=$((wu/60))
echo "Uptime WAN: ${wud}д ${wuh}ч ${wum}м<br/>"