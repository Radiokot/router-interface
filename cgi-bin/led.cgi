#!/bin/sh
#включает/выключает светодиод qss
#?switch - переключить, ?state - состояние
state() {
	LED="/sys/devices/platform/leds-gpio/leds/tp-link:green:qss/brightness"
	enable=$(cat $LED)
	if [ $enable -eq 1 ]
	then
	  echo '<b style="color:#1EFF00">█</b>'
	else
		echo '<b>█</b>'
	fi
	exit
}  
switch() {
	LED="/sys/devices/platform/leds-gpio/leds/tp-link:green:qss/brightness"
	enable=$(cat $LED) 
	enable=$((enable-1))
	enable=${enable#-}
	echo $enable > $LED
	
	state
	exit
}  

Q=$QUERY_STRING

echo "Content-type: text/html"
echo ""
if [ "$Q" == "state" ];
then
	state
else
	switch
fi
exit
